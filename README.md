# Testcases

Example testcases for SQLAlchemy + pytest + unittest.

## Installation
```
pip install -r requirements.txt
```

## Testing

```
pytest
```