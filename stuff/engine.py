import sqlalchemy
import os


def create_engine_from_os():
    return sqlalchemy.create_engine(os.environ['DATABASE_URL'])
