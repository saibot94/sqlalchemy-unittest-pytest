from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import String, Integer, Column

Base = declarative_base()


class Potato(Base):
    __tablename__ = 'potatoes'

    id = Column(Integer(), primary_key=True)
    name = Column(String(300))
