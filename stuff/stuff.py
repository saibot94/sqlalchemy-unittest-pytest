from stuff.models import Potato


def add(a, b):
    return a + b


def create_potato(name):
    return Potato(name=name)


class ActualDataThing:
    def __init__(self, settings):
        self.settings = settings

    def get_potato(self, is_good=True):
        return {
            "potato": {
                "good": is_good
            }
        }


class BloombergDataThing:
    def __init__(self):
        self.connection = ActualDataThing({"secret": "123"})

    def get_data(self, is_good):
        return self.connection.get_potato(is_good)
