import pathlib
import tempfile

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from stuff.models import Base


@pytest.fixture(scope="class")
def db_class(request):
    with tempfile.TemporaryDirectory() as tmpdirname:
        db_path = pathlib.Path(tmpdirname) / "test.db"
        print(f'db: {db_path}')
        engine = create_engine(f"sqlite:///{db_path}")
        # Base.metadata.create_all(engine)
        maker = sessionmaker(bind=engine)

        request.cls.engine = engine
        request.cls.session = maker()
        yield

        Base.metadata.drop_all(engine)
        db_path.unlink()
