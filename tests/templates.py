import pathlib
import tempfile

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from stuff.models import Base
import unittest


class DbTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        tmpdirname = tempfile.TemporaryDirectory()
        db_path = pathlib.Path(tmpdirname.name) / "test.db"
        print(f'db: {db_path}')
        engine = create_engine(f"sqlite:///{db_path}")
        maker = sessionmaker(bind=engine)
        cls.tmpdirname = tmpdirname
        cls.engine = engine
        cls.session = maker()

    @classmethod
    def tearDownClass(cls):
        Base.metadata.drop_all(cls.engine)
        cls.tmpdirname.cleanup()

    def setUp(self):
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        Base.metadata.drop_all(self.engine)
