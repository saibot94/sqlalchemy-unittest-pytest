import unittest
from unittest.mock import patch

from stuff.stuff import BloombergDataThing


class MockDataThing:
    data = {
        "potato": {
            "good": True
        }
    }

    def __init__(self, settings):
        self.settings = settings

    def get_potato(self, is_good):
        self.is_good = is_good
        return self.data


@patch('stuff.stuff.ActualDataThing')
class TestMockProvider(unittest.TestCase):

    def test_init_external_shit(self, dataclassmock):
        dataclassmock.return_value = MockDataThing({"secret": "123"})
        data_thing = BloombergDataThing()
        self.assertEqual(data_thing.get_data(True), MockDataThing.data)
        self.assertEqual(data_thing.connection.settings, {"secret": "123"})
