import unittest
from stuff.models import Potato, Base
from stuff.stuff import add, create_potato
from .templates import DbTest


class TestStuff(DbTest):

    def test_addd_1_2(self):
        self.assertEqual(add(1, 2), 3)

    def test_addd_fails(self):
        self.assertEqual(add(2, 2), 4)

    def test_add_stuff_to_db(self):
        self.session.add(create_potato("Cristi"))
        self.session.commit()

        self.assertIsNotNone(self.session.query(
            Potato).filter_by(name='Cristi').first())

    def test_2_db_stuff(self):
        self.assertEqual(len(self.session.query(Potato).all()), 0)

        self.session.add(create_potato("Cristi"))
        self.session.commit()
        self.assertIsNotNone(self.session.query(
            Potato).filter_by(name='Cristi').first())
